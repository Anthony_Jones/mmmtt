package com.mmmtt.resultant;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.*;

@RestController
@RequestMapping("message")
public class MessageController {
    public List<Map<String, String>> messages = new ArrayList<Map<String, String>>() {{
        add(new HashMap<String,String>(){{put("id", "1"); put("text", "First message");}});
        add(new HashMap<String,String>(){{put("id", "2"); put("text", "Second message");}});
        add(new HashMap<String,String>(){{put("id", "3"); put("text", "Third message");}});
    }};

    @GetMapping
    public List<Map<String, String>> list() {
        return this.messages;
    }
        @PostMapping
        public List<Map<String, Double>> compute(@RequestBody List<Map<String, String>> forces) {
            List<Force> forces1 = new ArrayList<>();
            for (Map<String, String> force:
                 forces) {
                Force forcetemp = new Force();
                if(force.get("type").equals("coords")) {
                    forcetemp = forcetemp.FromTwoCoords(Double.parseDouble(force.get("x")), Double.parseDouble(force.get("y")));
                }
                forces1.add(forcetemp);
            }
            Double sumProjectionOnX = 0.;
            Double sumProjectionOnY = 0.;
            for (Force force:
                 forces1) {
                sumProjectionOnX += force.projectionOnOsi('x');
                sumProjectionOnY += force.projectionOnOsi('y');
            }
            Double resultantMod = Math.sqrt(sumProjectionOnX * sumProjectionOnX + sumProjectionOnY *sumProjectionOnY);
            Double resultantPhi = Math.acos(resultantMod / sumProjectionOnX);
            Force resultant = new Force();
            resultant = resultant.FromAngleAndMod(resultantPhi, resultantMod);
            List<Double> resultantCoords = resultant.getCoords();
            List<Map<String, Double>> response = new ArrayList<Map<String, Double>>() {{
                add(new HashMap<String,Double>(){{put("x", resultantCoords.get(0)); put("y", resultantCoords.get(1));}});
            }};
            return response;
        }
}
