package com.mmmtt.resultant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResultantApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResultantApplication.class, args);
	}

}
