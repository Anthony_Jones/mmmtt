package com.mmmtt.resultant;


import java.util.ArrayList;

public class Force {
    private ArrayList<Double> coords; // x,y,z
    private Double phi;  // угол между Веткором и осью Х. от 0 до 2Pi
    private Double theta; // угол между Вектором и осью Z. от 0 до Pi
    private Double mod;

    public Force(ArrayList<Double> coords) {
        this.coords = coords;
        this.mod = this.mod();
        phi = Math.acos(coords.get(0) / mod);
        if(coords.get(2) != null) {
            theta = Math.acos(coords.get(2) / mod);
        }
    }

    public Force() {
    }

    public Double getMod() {
        return mod;
    }

    public Force FromThreeCoords(Double x, Double y, Double z) {
        Initializer(x, y, z);
        theta = Math.signum(coords.get(1)) * Math.acos(coords.get(2) / mod);
        return this;
    }

    public Force FromTwoCoords(Double x, Double y) {
        Initializer(x, y, 0.);
        this.theta = Math.PI / 2;
        return this;
    }

    private void Initializer(Double x, Double y, double z) {
        this.coords = new ArrayList<Double>();
        this.coords.add(x);
        this.coords.add(y);
        this.coords.add(z);
        this.mod = this.mod();
        phi = Math.signum(coords.get(1)) * Math.acos(coords.get(0) / mod);
    }


    public Force FromAngleAndMod(Double phi, Double mod) {
        this.phi = phi;
        this.mod = mod;
        this.coords = new ArrayList<Double>();
        this.theta = Math.PI / 2;
        coords.add(Math.cos(phi) * mod);
        coords.add(Math.sqrt(mod * mod - coords.get(0) * coords.get(0)));
        return this;
    }
    public ArrayList<Double> getCoords() {
        return coords;
    }

    public void setCoords(ArrayList<Double> coords) {
        this.coords = coords;
    }

    public Double getPhi() {
        return phi;
    }

    public void setPhi(Double phi) {
        this.phi = phi;
    }

    public Double getTheta() {
        return theta;
    }

    public void setTheta(Double theta) {
        this.theta = theta;
    }

    public Double scalarMultiplication(Force force) {
        ArrayList<Double> coords = force.getCoords();
        ArrayList<Double> thisCoords = this.getCoords();
        double result = 0.;
        for (int i = 0; i < coords.size(); i++) {
            result += coords.get(i) * thisCoords.get(i);
        }
        return result;
    }

    private Double mod() {
        ArrayList<Double> thisCoords = this.getCoords();
        double result = 0.;
        for (Double thisCoord : thisCoords) {
            result += thisCoord * thisCoord;
        }
        return Math.sqrt(result);
    }

    public Double angle(Force force) {
        return this.scalarMultiplication(force)/ (this.mod* force.mod);
    }

    public Double projectionOnOsi(Character xyz) {
        if(xyz == 'x') {
            return mod * Math.cos(phi);
        } else if(xyz == 'z') {
            return mod * Math.cos(theta);
        } else if(xyz == 'y') {
            if(Math.abs(Math.signum(coords.get(1))) > 10e-50 ) {
                return mod * Math.cos(Math.signum(coords.get(0)*coords.get(1))*(Math.PI / 2. - phi));
            }
            return 0.;
        } else {
            return 0.;
        }
    }
}
